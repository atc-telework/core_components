import Vue from "vue";
import _ from "lodash";

const state = {
	map: null
};

const mutations = {
	SET_MAP: (state, map) => {
		state.map = map;
	}
};

const actions = {};

export default {
	namespaced: true,
	state,
	actions,
	mutations
};
