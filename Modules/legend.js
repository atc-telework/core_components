import Vue from "vue";
const d3 = require("d3");
import _ from "lodash";

const state = {
	selected: {},
	legendUpdated: null
};

const getters = {
	selected: state => state.selected
};

const mutations = {
	SET_SELECTED: (state, obj) => {
		Vue.set(state.selected, obj.id, obj.data);
	},
	SET_LEGEND_UPDATED: (state, legend) => {
		state.legendUpdated = legend;
	}
};

const actions = {
	createSelected: ({ commit, state, rootState }, obj) => {
		console.log("obj", obj);
		commit("SET_SELECTED", obj);
		commit("SET_LEGEND_UPDATED", obj.id);
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
