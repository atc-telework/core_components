import Vue from "vue";
import _ from "lodash";
const axios = require("axios").default;

const state = {
	arcDataLoaded: false
};

const mutations = {
	SET_ARC_LOAD_STATE(state, bool) {
		state.arcDataLoaded = bool;
	}
};

const actions = {
	styleData: async ({ commit, rootState, dispatch }, obj) => {
		console.log("arcObj ---->", obj);
		axios({
			method: "get",
			withCredentials: true,
			url: obj.options.url + "?f=pjson",
			responseType: "json"
		}).then(function(response) {
			let style = response.data.drawingInfo.renderer.uniqueValueInfos;
			obj.data.forEach(elementOne => {
				style.forEach(elementTwo => {
					if (elementOne.properties[obj.style.iconProp] == elementTwo.value) {
						elementOne.properties.arcIcon = {
							type: elementTwo.symbol.contentType,
							icon: elementTwo.symbol.imageData,
							label: elementTwo.label
						};
					}
					if (!elementOne.properties[obj.style.iconProp]) {
						elementOne.properties.arcIcon = {
							type: "null",
							icon:
								"iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAA71JREFUSInVlk1sVFUUgL9z35vEkv4ZJWamxUQMLQ2a0AwLaqk/UcTojFtjt0hrFKNCAjExtiTSKDFYUBdMbTQ26sKF0WkDIhTSVqBpZ6ELWbiQhDqDuhAF3UzfPS7mvukrTv+smnhW75577/nOufecc5/Pfyz+/xaoqkkgqaoJEUkBCafPGGN6/zGgtbZXRHrCsYjMmxeRHmstIXRVQGvtMRHpKitU0Wu/oVeuQH4GNjRjGhpxEa8eKCLJEGRPncSOHkcSt6NfT6EXz+N/ci5cWgg/VnukBUfGbN+B2b4D+9U49vQIctc2ZO1a54/mVwQsFosJ3/fjLqpcqFfVjDsuAOzEGMHhV/Heeg/78RDEYvMdWwqoql1AF5CM6AAKqpoVkXQZNn6W4M0+vCODyM31yG3xsh0RyS4KdCneC6QqzQPxaLLYsTPYo6/jHR3ENK6DIEDa2suLZ2dnF47QWpsGPi/D8z9gx86gU+fh5x9hfROmbRvS1o7U1mHPjmLfPoTpfxfT2Fja5HmYpubQRCEWi1W+QxfZMUfGnhghyBzGe+oF5Nk9SFUVevUX9MI5gs5HMY93oqdGMEcGMQ0NFY9CVbuj4xsj7AXiAPb4MHZiFP/9T5H6+vICiSegZRNy/4PY/bsxO3dHYTkgA6QdLGeMyUYBZaCLLgVgZ2awH2TwBj5Camsrem7uWA8H+wme60S2tiN1dSWHRDIOWlGiwHTYlvT0SczTL0ZhOVUdBnIuWVIAZkMT+lgnOjWJPPQwRLJ5SWC0nnT8S0xffzgsAN3GmLD+sqo6HRqXtg70wgSUgBSLxUQ0SRYERkVnvoPqmjDybAQWSjcwDUBdHXr9+pzBUoNYFrBcK7KuGf74HdasmeuX82VOd/0acsutC9lfGKiq+fAOZWsHevFbpOM+KL1xybClueQqF71OTyIbN5Wdjra+RYFEI2y/F9v3CtKaRKqrAaZVddg14TRh6Vz6HvvZh/hPngi3LgqbBwyCIOP7fg+AaW5Bt7QRvNGHt/clpKYGIBV9XO3ly9iX9+LtO4jM3ffygbFYLG+tzYhIFyKYXc9g3+kn2PkEZtfzsLEFqboJvforOj2JHXgNb98hTOnYAXLRX4klgQDGmG73QiBVVXh79mO/eQQdG0WHBtCfLiF33o3c8wD+0BdIYq6dqeqBpWB/ATrZAmSBOMZgNrfC5tZFjajqgRtb2LKBLssSqppl4ecplNxKYBWBEXBaVZOu5aUo1V7BQfLA8EpASwIdNEcp1XtXavhvAf8N+RPLyH3lb97+XgAAAABJRU5ErkJggg==",
							label: "null"
						};
					}
				});
			});
			const dataProp = rootState.config.data[obj.id].style.prop;
			commit(
				"dataLoader/SET_GROUP_DATA",
				{
					id: obj.id,
					data: _.groupBy(obj.data, x => _.get(x.properties, dataProp))
				},
				{ root: true }
			);
			dispatch("icons/createArcIconObj", obj.id, { root: true });
			commit("SET_ARC_LOAD_STATE", true);
			console.log("FIINNNNNNALLLLLLLYYYYYYYY");
		});
		await commit("dataLoader/SET_GEOJSON_DATA", { id: obj.id, data: obj.data }, { root: true });
	},
	loadArcData: async ({ commit, dispatch }, obj) => {
		await axios({
			method: "get",
			withCredentials: true,
			url:
				obj.options.url +
				"/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&having=&gdbVersion=&historicMoment=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=xyFootprint&resultOffset=&resultRecordCount=&returnTrueCurves=false&returnExceededLimitFeatures=false&quantizationParameters=&returnCentroid=false&sqlFormat=none&resultType=&featureEncoding=esriDefault&f=geojson",
			responseType: "json"
		}).then(function(response) {
			obj.data = response.data.features;
			dispatch("styleData", obj);
		});
	}
};

export default {
	namespaced: true,
	state,
	mutations,
	actions
};
