import Vue from "vue";
import _ from "lodash";
import dayjs from "dayjs";
const d3 = require("d3");

const state = {
	chartData: {}
};

const mutations = {
	SET_CHART_DATA(state, obj) {
		Vue.set(state.chartData, obj.id, obj.data);
	}
};

const actions = {
	parsePieData: async ({ commit }, obj) => {
		let data = d3
			.nest()
			.key(function(d) {
				return d.feature ? d.feature.properties[obj.prop] : d.properties[obj.prop];
			})
			.rollup(function(v) {
				return v.length;
			})
			.entries(obj.data);
		let arr = [];

		for (let i = 0; i < data.length; i++) {
			arr.push({ name: data[i].key, y: data[i].value });
		}
		commit("SET_CHART_DATA", {
			id: obj.id,
			data: arr
		});
	},
	parseBarStockData: async ({ commit }, obj) => {
		let data = d3
			.nest()
			.key(d => d.properties[obj.prop])
			.key(d => new Date(dayjs(d.properties[obj.dateProp]).format("M/D/YYYY")).getTime())
			.rollup(v => v.length)
			.entries(obj.data);
		let arr = [];

		for (let i = 0; i < data.length; i++) {
			arr.push({ name: data[i].key, data: [], date: true, type: "column" });
			for (let k = 0; k < data[i].values.length; k++) {
				arr[i].data.push([Number(data[i].values[k].key), data[i].values[k].value]);
			}
		}
		for (let j = 0; j < arr.length; j++) {
			arr[j].data.sort(function(a, b) {
				return a[0] - b[0];
			});
		}
		commit("SET_CHART_DATA", {
			id: obj.id,
			data: arr
		});
	}
};

export default {
	namespaced: true,
	state,
	mutations,
	actions
};
