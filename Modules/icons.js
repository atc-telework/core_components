import Vue from "vue";
const d3 = require("d3");

const state = {
	iconObj: {}
};

const getters = {
	dataLoaded: state => state.dataLoaded
};

const mutations = {
	SET_ICON_OBJ(state, obj) {
		Vue.set(state.iconObj, obj.id, obj.data)
	}
};

//let imageData = obj.properties.arcIcon
//<img src="'data:image/jpeg;base64,'+imagedata"

const actions = {
	createIconObj: async ({ commit, rootState, dispatch }, id) => {
		const obj = {};
		if (rootState.config.data[id].style.subProp) {
			rootState.config.data[id].style.styleObj.forEach(item => {
				obj[item.value] = item;
			});
			obj.DEFAULT = {
				value: "DEFAULT",
				color: "red",
				icon: "image-off-outline",
				size: "32px"
			};
			let payload = {
				id: id,
				data: obj
			};
			commit("SET_ICON_OBJ", payload);
		} else {
			Object.keys(rootState.dataLoader.appData.grouped[id]).forEach((item, index) => {
				if (rootState.config.data[id].style.styleObj.find(x => x.value === item)) {
					obj[item] = {
						...rootState.config.data[id].style.styleObj[index]
					};
				}
			});
			obj.DEFAULT = {
				value: "DEFAULT",
				color: "red",
				icon: "image-off-outline",
				size: "32px"
			};
			let payload = {
				id: id,
				data: obj
			};
			commit("SET_ICON_OBJ", payload);
		}
	},
	createArcIconObj: async ({ commit, rootState, dispatch }, id) => {
		const obj = {};
		Object.keys(rootState.dataLoader.appData.grouped[id]).forEach((item, index) => {
			obj[item] = {
				...rootState.dataLoader.appData.grouped[id][item][0].properties.arcIcon
			};
		});
		obj.DEFAULT = {
			value: "DEFAULT",
			color: "red",
			icon: "image-off-outline",
			size: "32px"
		};
		let payload = {
			id: id,
			data: obj
		};
		commit("SET_ICON_OBJ", payload);
	}
};

//dave - 314-803-5361

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
