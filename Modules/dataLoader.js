import Vue from "vue";
import _ from "lodash";
const d3 = require("d3");

const state = {
	appData: {
		json: {},
		geojson: {},
		grouped: {},
		filtered: {}
	},
	dataLoaded: false
};

const getters = {
	dataLoaded: state => state.dataLoaded
};

const mutations = {
	SET_JSON_DATA: (state, obj) => {
		Vue.set(state.appData.json, obj.id, obj.data);
	},
	SET_GEOJSON_DATA: (state, obj) => {
		Vue.set(state.appData.geojson, obj.id, obj.data);
	},
	SET_GROUP_DATA: (state, obj) => {
		Vue.set(state.appData.grouped, obj.id, obj.data);
	},
	SET_FILTERED_DATA: (state, obj) => {
		Vue.set(state.appData.filtered, obj.id, obj.data);
	},
	dataLoaded: (state, bool) => {
		state.dataLoaded = bool;
	}
};

const actions = {
	loadData: async ({ commit, dispatch }, obj) => {
		switch (obj.dataInput) {
			case "csv":
				await dispatch("loadCSVData", obj);
				await dispatch("formatData", obj);
				await dispatch("icons/createIconObj", obj.id, { root: true });

				break;
			case "arcgis":
				await dispatch("arcgis/loadArcData", obj, { root: true });
				break;

			default:
				break;
		}
	},
	formatData: async ({ commit, dispatch }, obj) => {
		switch (obj.options.dataOutput) {
			case "geojson":
				await dispatch("createGeojson", obj);
				break;

			default:
				break;
		}
	},
	loadCSVData: async ({ commit, dispatch }, obj) => {
		await d3.csv(obj.options.url).then(async data => {
			await commit("SET_JSON_DATA", { id: obj.id, data: data });
		});
	},
	createGeojson: async ({ commit, state, rootState, dispatch }, obj) => {
		obj.data = state.appData.json[obj.id];
		let geo = [];
		for (let i = 0; i < obj.data.length; i++) {
			let _lat = Number(obj.data[i][obj.options.lat]);
			let _long = Number(obj.data[i][obj.options.lng]);
			geo[i] = {
				type: "Feature",
				geometry: {
					type: "Point",
					coordinates: [_long, _lat]
				},
				properties: obj.data[i]
			};
		}
		commit("SET_GEOJSON_DATA", { id: obj.id, data: geo });
		obj.data = geo;
		dispatch("groupData", obj);
	},
	groupData: async ({ commit, state, rootState }, obj) => {
		console.log("group", obj);
		if (!obj.style.subProp) {
			// Simple Legend
			const dataProp = rootState.config.data[obj.id].style.prop;
			commit("SET_GROUP_DATA", {
				id: obj.id,
				data: _.groupBy(obj.data, x => _.get(x.properties, dataProp))
			});
		} else {
			console.log("OBJ", obj);
			// Legend with a subprop
			let data = d3
				.nest()
				.key(d => d.properties[obj.style.prop])
				.key(d => d.properties[obj.style.subProp])
				.entries(obj.data);
			let final = {};

			data.forEach(item => {
				final[item.key] = {};
				item.values.forEach(val => {
					final[item.key][val.key] = val.values;
				});
			});
			obj.data = final;
			commit("SET_GROUP_DATA", {
				id: obj.id,
				data: final
			});
		}
	},
	groupFilteredData: async ({ commit, state, rootState }, obj) => {
		if (!rootState.config.data[obj.id].style.subProp) {
			console.log("Simple");
			// Simple Legend
			const dataProp = rootState.config.data[obj.id].style.prop;
			commit("SET_FILTERED_DATA", {
				id: obj.id,
				data: _.groupBy(obj.data, x => _.get(x.properties, dataProp))
			});
		} else {
			console.log("subprop");
			// Legend with a subprop
			let data = d3
				.nest()
				.key(d => d.properties[rootState.config.data[obj.id].style.prop])
				.key(d => d.properties[rootState.config.data[obj.id].style.subProp])
				.entries(obj.data);
			let final = {};

			data.forEach(item => {
				final[item.key] = {};
				item.values.forEach(val => {
					final[item.key][val.key] = val.values;
				});
			});
			obj.data = final;
			commit("SET_FILTERED_DATA", {
				id: obj.id,
				data: final
			});
		}
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
