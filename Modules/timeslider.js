import Vue from "vue";
import _ from "lodash";
import dayjs from "dayjs";

const state = {
	dateRange: [],
	dataUpdated: false
};

const getters = {
	dataUpdated: state => state.dataUpdated
};

const mutations = {
	SET_DATE_RANGE: (state, arr) => {
		state.dateRange = arr;
	},
	SET_TIMESLIDER_DATA: (state, arr) => {
		state.timesliderData = arr;
	},
	UPDATE_DATA: (state, bool) => {
		state.dataUpdated = bool;
	}
};

const actions = {
	filterData: ({ commit, rootState, dispatch }) => {
		const slider = rootState.config.timeslider;
		for (let i = 0; i < slider.data.length; i++) {
			const arr = rootState.dataLoader.appData.geojson[slider.data[i]].filter(item => {
				let date = new Date(
					dayjs(item.properties[slider.prop[i]]).format("M/D/YYYY")
				).getTime();
				let start = new Date(state.dateRange[0]).getTime();
				let end = new Date(state.dateRange[1]).getTime();
				if (date >= start && date <= end) {
					return item;
				}
			});
			let obj = {
				id: slider.data[i],
				data: arr
			};
			dispatch("dataLoader/groupFilteredData", obj, { root: true });
		}
		commit("UPDATE_DATA", true);
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
